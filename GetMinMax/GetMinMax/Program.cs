﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMinMax
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = GetArray(22);
            Print(arr);
            Console.WriteLine();
            
            var result = GetMinMax(arr);
            Console.WriteLine(result.Min);
            Console.WriteLine(result.Max);
            Console.WriteLine();
            
            Console.ReadKey();
        }
        static int[] GetArray(int count)
        {
            Random rnd = new Random();
            int[] arr = new int[count];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(10, 100);
            }
            return arr;
        }
        static (int Min, int Max) GetMinMax(int[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i += 2)
            {
                if (arr[i] > arr[i + 1])
                {
                    Swap(arr, i, i + 1);
                }
            }
            int min = arr[0];
            for (int i = 2; i < arr.Length; i += 2)
            {
                if (min > arr[i])
                {
                    min = arr[i];
                }
            }
            int max = arr[1];
            for (int i = 3; i < arr.Length; i += 2)
            {
                if (max < arr[i])
                {
                    max = arr[i];
                }
            }
            if (arr.Length % 2 != 0)
            {
                int a = arr[arr.Length - 1];
                if (a > max)
                {
                    max = a;
                }
            }
            return (min, max);
        }
        static void Swap(int[] arr, int i1, int i2)
        {
            var temp = arr[i1];
            arr[i1] = arr[i2];
            arr[i2] = temp;
        }
        static void Print(int[] arr)
        {
            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }
        }
    }

}
